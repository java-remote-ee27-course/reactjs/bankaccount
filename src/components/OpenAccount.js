import { initialDeposit } from "./App";

export default function OpenAccount({ dispatch, isActive }) {
  return (
    <p>
      <button
        onClick={() => {
          dispatch({ type: "openAccount", payload: initialDeposit });
        }}
        disabled={isActive}
      >
        Open account
      </button>
    </p>
  );
}
