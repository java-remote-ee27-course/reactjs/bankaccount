export default function RequestLoan({
  dispatch,
  isActive,
  isLoan,
  loanAmount,
}) {
  function handleSetLoanAmount(e) {
    dispatch({ type: "setLoanAmount", payload: Number(e.target.value) });
  }
  function handleRequestLoan() {
    dispatch({ type: "requestLoan" });
  }
  return (
    <p>
      <input
        type="number"
        min={500}
        disabled={!isActive || isLoan}
        value={loanAmount}
        onChange={handleSetLoanAmount}
      />

      <button onClick={handleRequestLoan} disabled={!isActive || isLoan}>
        Request a loan
      </button>
    </p>
  );
}
