function Error({ errorMessage }) {
  return <div className={errorMessage ? "error" : ""}>{errorMessage}</div>;
}

export default Error;
