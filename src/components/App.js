/*
INSTRUCTIONS / CONSIDERATIONS:

1. Let's implement a simple bank account! It's similar to the example 
that I used as an analogy to explain how useReducer works, 
but it's simplified (we're not using account numbers here)

2. Use a reducer to model the following state transitions: 
openAccount, deposit, withdraw, requestLoan, payLoan, closeAccount. 
Use the `initialState` below to get started.

3. All operations (expect for opening account) can only be performed if isActive is true. 
If it's not, just return the original state object. 
You can check this right at the beginning of the reducer

4. When the account is opened, isActive is set to true. 
There is also a minimum deposit amount of 500 to open an account 
(which means that the balance will start at 500)

5. Customer can only request a loan if there is no loan yet. 
If that condition is met, the requested amount will be registered in the 'loan' state, 
and it will be added to the balance. If the condition is not met, just return the current state

6. When the customer pays the loan, the opposite happens: 
the money is taken from the balance, and the 'loan' will get back to 0. 
The loan cannot be paid back if the balance is smaller than the loan.
// Initial was: The loan can be paid back however much money there is on an account
// This can lead to negative balances, but that's no problem, 
// because the customer can't close their account now (see next point).

7. Customer can only close an account if there is no loan, AND if the balance is zero. 
If this condition is not met, just return the state. 
If the condition is met, the account is deactivated and all money is withdrawn. 
The account basically gets back to the initial state
*/

import { useReducer } from "react";
import Error from "./Error";
import OpenAccount from "./OpenAccount";
import Deposit from "./Deposit";
import WithDraw from "./WithDraw";
import RequestLoan from "./RequestLoan";
import PayLoan from "./PayLoan";
import CloseAccount from "./CloseAccount";

const initialState = {
  balance: 0,
  currentLoan: 0, //loan that user has
  isActive: false,
  isLoan: false,
  errorMessage: "",
  depositAmount: 0,
  withdrawAmount: 0,
  loanAmount: 500, //loan amount that the user wants to take
  loanPaybackAmount: 0,
};

export const initialDeposit = 500;

const errorMsg = {
  NOT_ENOUGH_MONEY: "Not enough money on the account! No transfer made.",
  BALANCE_NOT_ZERO: "Balance not 0, cannot close the account!",
  LOAN_BIGGER_THAN_ZERO: "The loan is bigger than 0, cannot close the account!",
  NOT_SUPPORTED: "Action not supported!",
  LOAN_AMOUNT_TOO_SMALL:
    "Loan amount too small, the amount must be at least 500!",
};

function reducer(state, action) {
  if (!state.isActive && action.type !== "openAccount") return state;
  state.errorMessage = "";

  switch (action.type) {
    case "openAccount":
      return {
        ...state,
        balance: action.payload,
        isActive: true,
      };
    case "setDepositAmount":
      return {
        ...state,
        depositAmount: action.payload,
      };
    case "deposit":
      //deposits the depositAmount (amount set in "setDepositAmount")
      return {
        ...state,
        balance: state.balance + state.depositAmount,
        depositAmount: 0,
      };
    case "setWithdrawAmount":
      return { ...state, withdrawAmount: action.payload };

    case "withdraw":
      if (state.balance <= 0 || state.balance < state.withdrawAmount)
        return {
          ...state,
          errorMessage: errorMsg.NOT_ENOUGH_MONEY,
        };
      return {
        ...state,
        balance: state.balance - state.withdrawAmount,
        withdrawAmount: 0,
      };

    case "setLoanAmount":
      return { ...state, loanAmount: action.payload };
    case "requestLoan":
      // if user has a loan, they cannot get another.
      if (state.currentLoan !== 0) return state;
      return {
        ...state,
        balance:
          state.loanAmount >= 500
            ? state.balance + state.loanAmount
            : state.balance,
        errorMessage:
          state.loanAmount >= 500 ? "" : errorMsg.LOAN_AMOUNT_TOO_SMALL,
        currentLoan: state.loanAmount >= 500 ? state.loanAmount : 0,
        isLoan: state.loanAmount >= 500 ? true : false,
        loanAmount: state.loanAmount >= 500 ? 0 : state.loanAmount,
      };
    case "setLoanPaybackAmount":
      return { ...state, loanPaybackAmount: action.payload };

    case "payLoan":
      //  check if user has a loan:
      if (state.currentLoan === 0) {
        return { ...state, isLoan: false };
      }
      // check if there are enough resources to pay the loan:
      if (state.balance < state.loanPaybackAmount) {
        return {
          ...state,
          errorMessage: errorMsg.NOT_ENOUGH_MONEY,
        };
      }

      return {
        ...state,
        balance:
          state.currentLoan >= state.loanPaybackAmount
            ? state.balance - state.loanPaybackAmount
            : state.balance - state.currentLoan,

        currentLoan:
          state.currentLoan >= state.loanPaybackAmount
            ? state.currentLoan - state.loanPaybackAmount
            : 0,
        isLoan: state.currentLoan - state.loanPaybackAmount > 0 ? true : false,
        loanPaybackAmount: 0,
      };
    case "closeAccount":
      if (state.balance !== 0)
        return {
          ...state,
          errorMessage: errorMsg.BALANCE_NOT_ZERO,
        };
      if (state.currentLoan !== 0)
        return {
          ...state,
          errorMessage: errorMsg.LOAN_BIGGER_THAN_ZERO,
        };
      return initialState;

    default:
      throw new Error(errorMsg.NOT_SUPPORTED);
  }
}
export default function App() {
  const [
    {
      balance,
      currentLoan,
      isActive,
      errorMessage,
      isLoan,
      depositAmount,
      withdrawAmount,
      loanAmount,
      loanPaybackAmount,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  return (
    <div className="app">
      {errorMessage && <Error errorMessage={errorMessage} />}
      <div className="page">
        <h1>useReducer Bank Account</h1>
        <p>Your balance: {balance}</p>
        <p>Your loan: {currentLoan}</p>

        <OpenAccount
          dispatch={dispatch}
          isActive={isActive}
          depositAmount={depositAmount}
        />
        <Deposit
          dispatch={dispatch}
          isActive={isActive}
          depositAmount={depositAmount}
        />
        <WithDraw
          dispatch={dispatch}
          isActive={isActive}
          withdrawAmount={withdrawAmount}
        />
        <RequestLoan
          dispatch={dispatch}
          isActive={isActive}
          isLoan={isLoan}
          loanAmount={loanAmount}
        />
        <PayLoan
          dispatch={dispatch}
          isActive={isActive}
          isLoan={isLoan}
          loanPaybackAmount={loanPaybackAmount}
        />
        <CloseAccount dispatch={dispatch} isActive={isActive} isLoan={isLoan} />
      </div>
    </div>
  );
}
