export default function WithDraw({ dispatch, isActive, withdrawAmount }) {
  function handleSetWithdrawAmount(e) {
    dispatch({ type: "setWithdrawAmount", payload: Number(e.target.value) });
  }

  function handleWithdraw() {
    dispatch({ type: "withdraw" });
  }
  return (
    <p>
      <input
        type="number"
        min={0}
        disabled={!isActive}
        value={withdrawAmount}
        onChange={handleSetWithdrawAmount}
      />
      <button onClick={handleWithdraw} disabled={!isActive}>
        Withdraw
      </button>
    </p>
  );
}
