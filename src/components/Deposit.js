export default function Deposit({ dispatch, isActive, depositAmount }) {
  const handleSetDeposit = function (e) {
    dispatch({ type: "setDepositAmount", payload: Number(e.target.value) });
  };
  const handleChangeDeposit = function () {
    dispatch({ type: "deposit" });
  };

  return (
    <p>
      <input
        type="number"
        min={0}
        disabled={!isActive}
        value={depositAmount}
        onChange={handleSetDeposit}
      />
      <button onClick={handleChangeDeposit} disabled={!isActive}>
        Deposit
      </button>
    </p>
  );
}
