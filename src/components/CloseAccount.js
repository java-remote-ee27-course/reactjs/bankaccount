export default function CloseAccount({ dispatch, isActive }) {
  return (
    <p>
      <button
        onClick={() => {
          dispatch({ type: "closeAccount" });
        }}
        disabled={!isActive}
      >
        Close account
      </button>
    </p>
  );
}
