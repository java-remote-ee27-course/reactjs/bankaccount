export default function PayLoan({
  dispatch,
  isActive,
  isLoan,
  loanPaybackAmount,
}) {
  function handleSetPayLoanAmount(e) {
    dispatch({ type: "setLoanPaybackAmount", payload: Number(e.target.value) });
  }
  function handlePayLoan() {
    dispatch({ type: "payLoan" });
  }
  return (
    <p>
      <input
        type="number"
        min={500}
        disabled={!isActive || !isLoan}
        value={loanPaybackAmount}
        onChange={handleSetPayLoanAmount}
      />
      <button onClick={handlePayLoan} disabled={!isActive || !isLoan}>
        Pay loan
      </button>
    </p>
  );
}
