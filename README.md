# useReducer Bank Account

1. useReducer hook models the following state transitions:

   - openAccount, deposit, withdraw, requestLoan, payLoan, closeAccount.

2. All operations (expect for opening account) can only be performed if isActive is true.

3. When the account is opened, initial deposit amount of 500 is required (balance is automatically set to 500 when opening the account)

4. Customer can only withdraw the money if there are enough resources on the account.

5. Customer can only request a loan if there is no loan yet.

   - If that condition is met, the requested amount will be registered in the 'loan' state,
     and it will be added to the balance.

6. When the customer pays the loan, the opposite happens:

   - the money is taken from the balance, and the 'loan' will get back to 0.
   - If customer tries to pay back bigger amount than the loan, then the loan amount will not go to negative, instead only as much money is taken from the account as it is required to pay back the loan.
   - The loan cannot be paid back if the balance is smaller than the loan.

7. Customer can only close an account if there is no loan, AND if the balance is zero.

   - If this condition is not met, just return the state.
   - If the condition is met, the account is deactivated.
   - The account basically gets back to the initial state

8. Customer can set the amounts that they want to deposit, withdraw, request as loan, and pay back.

## TODO:

- Refactor the code
- Run tests

## THE NEW VERSION:

![bankaccount](./public/bank11.png)
![bankaccount](./public/bank12.png)
![bankaccount](./public/bank12-2.png)

The loan amount is too small:

![bankaccount](./public/bank13.png)

Trying to pay back the loan if there is not enough money on the account:

![bankaccount](./public/bank14.png)

Trying to widthdraw if balance is smaller than the withdrawal amount:

![bankaccount](./public/bank15.png)

Trying to close the account if the balance is bigger than 0:

![bankaccount](./public/bank16.png)

Trying to close the account if the loan is bigger than 0:

![bankaccount](./public/bank17.png)

## My initial version of the app was:

![bankaccount](./public/bank1.png)
![bankaccount](./public/bank2.png)

Trying to pay back the loan if there is not enough money on the account:

![bankaccount](./public/bank3.png)

Trying to widthdraw if balance is 0:

![bankaccount](./public/bank4.png)

Trying to close the account if the balance is bigger than 0:

![bankaccount](./public/bank5.png)

Trying to close the account if the loan is bigger than 0:

![bankaccount](./public/bank6.png)

## Author of the code and styles

Katlin
